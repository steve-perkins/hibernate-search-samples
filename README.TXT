* "chapter1" represents the state of the example application as of the end of the first chapter.  
  This is a Maven-based project, containing a version of the application using the 
  annotation-based (JPA-style) mappings.
  
* "chapter1-ant" is a variant of "chapter1", setup to be built by Ant/Ivy rather than Maven.

* "chapter2" represents the state of the example application as of the end of the second chapter.
  This is a Maven-based project, containing a version of the application using the 
  annotation-based (JPA-style) mappings.
  
* "chapter2-xml" is a variant of "chapter2", using classic Hibernate XML-style mappings rather 
  than JPA annotations.
  
TODO
  
* "chapter2-mapping"   

* "chapter3" represents the state of the example application as of the end of the third chapter.
  This is a Maven-based project, containing a version of the application using annotation-based
  mappings and Hibernate session-based queries.
  
* "chapter3-entitymanager" is a variant of "chapter3", using queries based on a JPA entity manager 
  rather than a Hibernate session.

TODO

* "chapter4" 

* "chapter5" 

* "chapter6" 

* "chapter7" 
  